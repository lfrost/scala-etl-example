-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.2-beta1
-- PostgreSQL version: 11.0
-- Project Site: pgmodeler.io
-- Model Author: Lyle Frost <lfrost@cnz.com>

-- -- object: etl_datamart | type: ROLE --
-- -- DROP ROLE IF EXISTS etl_datamart;
-- CREATE ROLE etl_datamart WITH 
-- 	INHERIT
-- 	LOGIN;
-- -- ddl-end --
-- 

-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: etl_datamart | type: DATABASE --
-- -- DROP DATABASE IF EXISTS etl_datamart;
-- CREATE DATABASE etl_datamart
-- 	OWNER = etl_datamart;
-- -- ddl-end --
-- 

-- -- object: citext | type: EXTENSION --
-- -- DROP EXTENSION IF EXISTS citext CASCADE;
-- CREATE EXTENSION citext
-- WITH SCHEMA public;
-- -- ddl-end --
-- 
-- object: public."DailySalesFact" | type: TABLE --
-- DROP TABLE IF EXISTS public."DailySalesFact" CASCADE;
CREATE TABLE public."DailySalesFact" (
	id bigserial NOT NULL,
	cents integer NOT NULL,
	units smallint NOT NULL,
	"idDateDimension" smallint NOT NULL,
	"idProductDimension" uuid NOT NULL,
	"idStoreDimension" smallint NOT NULL,
	CONSTRAINT "pkDailySalesFact" PRIMARY KEY (id)

);
-- ddl-end --

-- object: public."DateDimension" | type: TABLE --
-- DROP TABLE IF EXISTS public."DateDimension" CASCADE;
CREATE TABLE public."DateDimension" (
	id smallserial NOT NULL,
	date date NOT NULL,
	"dayOfWeek" smallint NOT NULL,
	month smallint NOT NULL,
	year smallint NOT NULL,
	CONSTRAINT "pkDateDimension" PRIMARY KEY (id),
	CONSTRAINT "dateUnique" UNIQUE (date),
	CONSTRAINT "dayOfWeekValid" CHECK ("dayOfWeek" BETWEEN 1 AND 7),
	CONSTRAINT "monthValid" CHECK ("month" BETWEEN 1 AND 12)

);
-- ddl-end --

-- object: public."ProductDimension" | type: TABLE --
-- DROP TABLE IF EXISTS public."ProductDimension" CASCADE;
CREATE TABLE public."ProductDimension" (
	id uuid NOT NULL,
	gtin character(13) NOT NULL,
	name text NOT NULL,
	brand text,
	cents integer NOT NULL,
	CONSTRAINT "pkProductDimension" PRIMARY KEY (id),
	CONSTRAINT "gtinUnique" UNIQUE (gtin)

);
-- ddl-end --

-- object: public."StoreDimension" | type: TABLE --
-- DROP TABLE IF EXISTS public."StoreDimension" CASCADE;
CREATE TABLE public."StoreDimension" (
	id smallserial NOT NULL,
	code text NOT NULL,
	name text NOT NULL,
	city text NOT NULL,
	"postalCode" text NOT NULL,
	region text NOT NULL,
	CONSTRAINT "pkStoreDimension" PRIMARY KEY (id),
	CONSTRAINT "codeUnique" UNIQUE (code)

);
-- ddl-end --

-- object: "fkDateDimension" | type: CONSTRAINT --
-- ALTER TABLE public."DailySalesFact" DROP CONSTRAINT IF EXISTS "fkDateDimension" CASCADE;
ALTER TABLE public."DailySalesFact" ADD CONSTRAINT "fkDateDimension" FOREIGN KEY ("idDateDimension")
REFERENCES public."DateDimension" (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;
-- ddl-end --

-- object: "ndxIdDateDimension" | type: INDEX --
-- DROP INDEX IF EXISTS public."ndxIdDateDimension" CASCADE;
CREATE INDEX "ndxIdDateDimension" ON public."DailySalesFact"
	USING btree
	(
	  "idDateDimension"
	);
-- ddl-end --

-- object: "fkProductDimension" | type: CONSTRAINT --
-- ALTER TABLE public."DailySalesFact" DROP CONSTRAINT IF EXISTS "fkProductDimension" CASCADE;
ALTER TABLE public."DailySalesFact" ADD CONSTRAINT "fkProductDimension" FOREIGN KEY ("idProductDimension")
REFERENCES public."ProductDimension" (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;
-- ddl-end --

-- object: "ndxIdProductDimension" | type: INDEX --
-- DROP INDEX IF EXISTS public."ndxIdProductDimension" CASCADE;
CREATE INDEX "ndxIdProductDimension" ON public."DailySalesFact"
	USING btree
	(
	  "idProductDimension"
	);
-- ddl-end --

-- object: "fkStoreDimension" | type: CONSTRAINT --
-- ALTER TABLE public."DailySalesFact" DROP CONSTRAINT IF EXISTS "fkStoreDimension" CASCADE;
ALTER TABLE public."DailySalesFact" ADD CONSTRAINT "fkStoreDimension" FOREIGN KEY ("idStoreDimension")
REFERENCES public."StoreDimension" (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;
-- ddl-end --

-- object: "ndxIdStoreDimension" | type: INDEX --
-- DROP INDEX IF EXISTS public."ndxIdStoreDimension" CASCADE;
CREATE INDEX "ndxIdStoreDimension" ON public."DailySalesFact"
	USING btree
	(
	  "idStoreDimension"
	);
-- ddl-end --


