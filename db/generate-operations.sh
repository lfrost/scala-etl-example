#!/bin/bash -u
#
# Construct CLASSPATH and run Scala script.
#
# Author Lyle Frost <lfrost@cnz.com>
#

set -o pipefail

declare -ir EXIT_SUCCESS=0
declare -ir EXIT_FAILURE=1
declare -ir EXIT_USAGE=2

declare -r SCRIPT_DIR="${0%/*}"

if ! cd "$SCRIPT_DIR"
then
    echo "Error changing directory to $SCRIPT_DIR."
    exit $EXIT_FAILURE
fi

if ! [ -f dbpass ]
then
    echo 'Before running this script, create a database password file named "dbpass".'
    exit $EXIT_FAILURE
fi

# Usage:  listJoin delimiter s1 ...
function listJoin {
    if (($# < 2))
    then
        exit $EXIT_USAGE
    fi
    local delimiter="$1"
    local s1="$2"
    shift 2

    echo -n "$s1"
    for s in "$@"
    do
        echo -n "$delimiter$s"
    done
}

if ! JAR_LIST=$(ls -1 lib/*.jar)
then
    echo "Error listing jars in library directory."
    exit 1
fi

CLASSPATH="$(listJoin ':' $JAR_LIST)"                       \
PGPASSWORD=$(< dbpass)                                      \
    scala generate-operations.scala                         \
        'jdbc:postgresql://localhost:5432/etl_operations'   \
        etl_operations                                      \
        100 10000
