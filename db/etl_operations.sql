-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.2-beta1
-- PostgreSQL version: 11.0
-- Project Site: pgmodeler.io
-- Model Author: Lyle Frost <lfrost@cnz.com>

-- -- object: etl_operations | type: ROLE --
-- -- DROP ROLE IF EXISTS etl_operations;
-- CREATE ROLE etl_operations WITH 
-- 	INHERIT
-- 	LOGIN;
-- -- ddl-end --
-- 

-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: etl_operations | type: DATABASE --
-- -- DROP DATABASE IF EXISTS etl_operations;
-- CREATE DATABASE etl_operations
-- 	OWNER = etl_operations;
-- -- ddl-end --
-- 

-- -- object: citext | type: EXTENSION --
-- -- DROP EXTENSION IF EXISTS citext CASCADE;
-- CREATE EXTENSION citext
-- WITH SCHEMA public;
-- -- ddl-end --
-- 
-- object: public."Product" | type: TABLE --
-- DROP TABLE IF EXISTS public."Product" CASCADE;
CREATE TABLE public."Product" (
	id uuid NOT NULL DEFAULT gen_random_uuid(),
	"whenCreated" timestamptz DEFAULT transaction_timestamp(),
	gtin character(13) NOT NULL,
	name citext NOT NULL,
	brand citext,
	cents integer NOT NULL,
	CONSTRAINT "pkProduct" PRIMARY KEY (id),
	CONSTRAINT "gtinUnique" UNIQUE (gtin)

);
-- ddl-end --
COMMENT ON COLUMN public."Product".gtin IS 'global trade item number';
-- ddl-end --

-- object: public."Transaction" | type: TABLE --
-- DROP TABLE IF EXISTS public."Transaction" CASCADE;
CREATE TABLE public."Transaction" (
	id uuid NOT NULL DEFAULT gen_random_uuid(),
	"whenCreated" timestamptz DEFAULT transaction_timestamp(),
	"idStore" uuid NOT NULL,
	CONSTRAINT "pkTransaction" PRIMARY KEY (id)

);
-- ddl-end --

-- object: public."TransactionItem" | type: TABLE --
-- DROP TABLE IF EXISTS public."TransactionItem" CASCADE;
CREATE TABLE public."TransactionItem" (
	id uuid NOT NULL DEFAULT gen_random_uuid(),
	cents integer NOT NULL,
	units smallint NOT NULL,
	"idTransaction" uuid NOT NULL,
	"idProduct" uuid NOT NULL,
	CONSTRAINT "pkTransactionItem" PRIMARY KEY (id)

);
-- ddl-end --

-- object: public."Store" | type: TABLE --
-- DROP TABLE IF EXISTS public."Store" CASCADE;
CREATE TABLE public."Store" (
	id uuid NOT NULL DEFAULT gen_random_uuid(),
	"whenCreated" timestamptz NOT NULL DEFAULT transaction_timestamp(),
	code text NOT NULL,
	name citext NOT NULL,
	city citext NOT NULL,
	"postalCode" text NOT NULL,
	region citext NOT NULL,
	CONSTRAINT "pkStore" PRIMARY KEY (id),
	CONSTRAINT "codeUnique" UNIQUE (code)

);
-- ddl-end --

-- object: "fkStore" | type: CONSTRAINT --
-- ALTER TABLE public."Transaction" DROP CONSTRAINT IF EXISTS "fkStore" CASCADE;
ALTER TABLE public."Transaction" ADD CONSTRAINT "fkStore" FOREIGN KEY ("idStore")
REFERENCES public."Store" (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;
-- ddl-end --

-- object: "fkTransaction" | type: CONSTRAINT --
-- ALTER TABLE public."TransactionItem" DROP CONSTRAINT IF EXISTS "fkTransaction" CASCADE;
ALTER TABLE public."TransactionItem" ADD CONSTRAINT "fkTransaction" FOREIGN KEY ("idTransaction")
REFERENCES public."Transaction" (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;
-- ddl-end --

-- object: "fkProduct" | type: CONSTRAINT --
-- ALTER TABLE public."TransactionItem" DROP CONSTRAINT IF EXISTS "fkProduct" CASCADE;
ALTER TABLE public."TransactionItem" ADD CONSTRAINT "fkProduct" FOREIGN KEY ("idProduct")
REFERENCES public."Product" (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;
-- ddl-end --

-- object: "ndxIdStore" | type: INDEX --
-- DROP INDEX IF EXISTS public."ndxIdStore" CASCADE;
CREATE INDEX "ndxIdStore" ON public."Transaction"
	USING btree
	(
	  "idStore"
	);
-- ddl-end --

-- object: "ndxIdTransaction" | type: INDEX --
-- DROP INDEX IF EXISTS public."ndxIdTransaction" CASCADE;
CREATE INDEX "ndxIdTransaction" ON public."TransactionItem"
	USING btree
	(
	  "idTransaction"
	);
-- ddl-end --

-- object: "ndxIdProduct" | type: INDEX --
-- DROP INDEX IF EXISTS public."ndxIdProduct" CASCADE;
CREATE INDEX "ndxIdProduct" ON public."TransactionItem"
	USING btree
	(
	  "idProduct"
	);
-- ddl-end --


