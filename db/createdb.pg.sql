-- ---------------------------------------------------------------------
-- Database Creation
--
-- Author Lyle Frost <lfrost@cnz.com>
--
-- See the README for documentation.
--
-- ---------------------------------------------------------------------

\set ON_ERROR_STOP

\if :{?password}
\else
    DO LANGUAGE plpgsql $$
    BEGIN
        RAISE EXCEPTION 'Variable "password" not defined.';
    END
    $$;
\endif

\set dbname etl_operations

\set username :dbname

CREATE ROLE :"username"
    NOCREATEDB
    NOCREATEROLE
    INHERIT
    LOGIN
    NOSUPERUSER
    PASSWORD :'password';

CREATE DATABASE :"dbname"
    OWNER :"username"
    ENCODING 'UNICODE';

\connect :dbname

CREATE EXTENSION "adminpack";

CREATE EXTENSION "citext";

CREATE EXTENSION "pgcrypto";

\connect postgres

\set dbname etl_datamart

\set username :dbname

CREATE ROLE :"username"
    NOCREATEDB
    NOCREATEROLE
    INHERIT
    LOGIN
    NOSUPERUSER
    PASSWORD :'password';

CREATE DATABASE :"dbname"
    OWNER :"username"
    ENCODING 'UNICODE';

\connect :dbname

CREATE EXTENSION "adminpack";

-- eof
