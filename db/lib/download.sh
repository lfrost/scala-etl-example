#!/bin/bash -fu
#
# Download the jars needed by the generate-operations script.
#

set -o pipefail

declare -ir EXIT_SUCCESS=0
declare -ir EXIT_FAILURE=1
declare -ir EXIT_USAGE=2

declare -r SCRIPT_DIR="${0%/*}"

if ! cd "$SCRIPT_DIR"
then
    echo "Error changing directory to $SCRIPT_DIR."
    exit $EXIT_FAILURE
fi

FILE_LIST='
    org/playframework/anorm/anorm_2.13/2.6.10/anorm_2.13-2.6.10.jar
    org/playframework/anorm/anorm-tokenizer_2.13/2.6.10/anorm-tokenizer_2.13-2.6.10.jar
    dk/brics/automaton/1.12-1/automaton-1.12-1.jar
    org/apache/commons/commons-lang3/3.12.0/commons-lang3-3.12.0.jar
    com/github/mifmif/generex/1.0.2/generex-1.0.2.jar
    com/github/javafaker/javafaker/1.0.2/javafaker-1.0.2.jar
    org/postgresql/postgresql/42.2.23/postgresql-42.2.23.jar
    org/scala-lang/modules/scala-parser-combinators_2.13/2.0.0/scala-parser-combinators_2.13-2.0.0.jar
    org/yaml/snakeyaml/1.29/snakeyaml-1.29.jar
'

for file in $FILE_LIST
do
    url="https://repo1.maven.org/maven2/$file"
    if ! wget "$url"
    then
        echo "Error downloading $file."
        exit $EXIT_FAILURE
    fi
done

exit $EXIT_SUCCESS
