/*
 * Script to generate dummy data for the ETL operations database.
 *
 * Copyright (c) 2019-2021 Lyle Frost <lfrost@cnz.com>.
 */

import java.security.SecureRandom
import java.sql.{Connection => DbConnection, DriverManager}
import java.time.{ZonedDateTime, ZoneId}
import java.util.{Calendar, GregorianCalendar, UUID}
import java.util.concurrent.TimeUnit

import anorm.{SQL, SqlParser}
import com.github.javafaker.Faker

val exitSuccess = 0
val exitFailure = 1
val exitUsage   = 2
val usage       = "Usage: generate-operations.scala url username nProducts nTransactions"

val progressInterval = 1000
val progressChar     = '.'

// Process the command line.
if (args.length != 4) {
    Console.println(usage)
    sys.exit(exitUsage)
}
val url      = args(0)
val username = args(1)
val password = sys.env.get("PGPASSWORD") match {
    case None => {
        Console.println(usage)
        Console.println("Environment variable PGPASSWORD not defined.")
        sys.exit(exitUsage)
    }
    case Some(p) => p
}
val nProducts : Int = try { args(2).toInt } catch {
    case _ : Exception => {
        Console.println(usage)
        Console.println("nProducts must be an integer.")
        sys.exit(exitUsage)
    }
}
val nTransactions : Int = try { args(3).toInt } catch {
    case _ : Exception => {
        Console.println(usage)
        Console.println("nTransactions must be an integer.")
        sys.exit(exitUsage)
    }
}

// Create a random number generator and fakers.
val random = new SecureRandom()
def randomInt(min:Int, max:Int) = random.nextInt(max + 1 - min) + min
val faker         = new Faker()
val codeFaker     = faker.code()
val commerceFaker = faker.commerce()
val loremFaker    = faker.lorem()
val dateFaker     = faker.date()

// Connect to the database.
implicit val conn = try {
    Class.forName("org.postgresql.Driver")
    DriverManager.getConnection(url, username, password)
} catch {
    case e : Exception => {
        Console.println(s"Failed to connect to $url:  ${e.getClass.getSimpleName}:  ${e.getMessage}")
        sys.exit(exitFailure)
    }
}

Console.println(">>>> Loading Store table.")

case class Store(
    code       : String,
    name       : String,
    city       : String,
    postalCode : String,
    region     : String
)

val stores = List(
    Store("OH001", "Store 1", "Cincinnati", "45242", "us-midwest"),
    Store("OH002", "Store 2", "Cleveland",  "44115", "us-midwest"),
    Store("IL001", "Store 3", "Chicago",    "60601", "us-midwest"),
    Store("NY001", "Store 4", "New York",   "10001", "us-east"),
    Store("GA001", "Store 5", "Atlanta",    "30301", "us-south")
)

/* Insert a store record for each of store and build list of store IDs
 * for use later in script.
 */
val storeIds : Seq[UUID] = stores.foldLeft(Seq.empty[UUID]) { (list, store) =>
    try {
        SQL(s"""
            |INSERT INTO "Store"
            |    ("code", "name", "city", "postalCode", "region")
            |VALUES
            |    ({code}, {name}, {city}, {postalCode}, {region})
        """.stripMargin.trim).
            asSimple().
            on(
                "code"       -> store.code,
                "name"       -> store.name,
                "city"       -> store.city,
                "postalCode" -> store.postalCode,
                "region"     -> store.region
            ).
            executeInsert(SqlParser.scalar[UUID].single) +: list
    } catch {
        case e : Exception => {
            Console.println(s"Failed to insert store ${store.code}:  ${e.getClass.getSimpleName}:  ${e.getMessage}")
            sys.exit(exitFailure)
        }
    }
}

Console.println(">>>> Loading Product table.")

/* Insert nProducts generated products with faked GTIN, name, and brand,
 * and random cost in range [minCents, maxCents].  Build a list of
 * products containing the ID and cents for use later in the script.
 */
val minCents = 10
val maxCents = 20000
val products : Seq[(UUID, Int)] = (1 to nProducts).foldLeft(Seq.empty[(UUID, Int)]) { (list, i) =>
    val gtin  = codeFaker.gtin13()
    val name  = commerceFaker.productName()
    val brand = Some(loremFaker.word())
    val cents = randomInt(minCents, maxCents)

    try {
        val id = SQL(s"""
            |INSERT INTO "Product"
            |    ("gtin", "name", "brand", "cents")
            |VALUES
            |    ({gtin}, {name}, {brand}, {cents})
        """.stripMargin.trim).
            asSimple().
            on(
                "gtin"  -> gtin,
                "name"  -> name,
                "brand" -> brand,
                "cents" -> cents
            ).
            executeInsert(SqlParser.scalar[UUID].single)

        (id, cents) +: list
    } catch {
        case e : Exception => {
            Console.println(s"Failed to insert product ${gtin}:  ${e.getClass.getSimpleName}:  ${e.getMessage}")
            sys.exit(exitFailure)
        }
    }
}

Console.println(">>>> Loading Transaction tables.")

/* Insert nTransactions generated transactions each at a randomly
 * selected store, with random number of items in range [minItems,
 * maxItems], and each item with a randomly selected product and a
 * random number of units in range [minUnits, maxUnits].
 */
val minItems = 1
val maxItems = 20
val minUnits = 1
val maxUnits = 20
val years = 5
val cal = new GregorianCalendar()
1 to nTransactions foreach { i =>
    // Echo a progress dot periodically.
    if (i % progressInterval == 0) Console.print(progressChar)

    cal.setTime(dateFaker.past(years * 365, TimeUnit.DAYS))
    val whenCreated = ZonedDateTime.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND), 0, ZoneId.of("UTC"))

    val idStore = storeIds(random.nextInt(storeIds.length))

    val idTransaction = try {
        SQL(s"""
            |INSERT INTO "Transaction"
            |    ("whenCreated", "idStore")
            |VALUES
            |    ({whenCreated}, {idStore}::uuid)
        """.stripMargin.trim).
            asSimple().
            on(
                "whenCreated" -> whenCreated,
                "idStore"     -> idStore
            ).
            executeInsert(SqlParser.scalar[UUID].single)
    } catch {
        case e : Exception => {
            Console.println(s"Failed to insert transaction #$i:  ${e.getClass.getSimpleName}:  ${e.getMessage}")
            sys.exit(exitFailure)
        }
    }

    val nItems = randomInt(minItems, maxItems)
    1 to nItems foreach { i =>
        val product = products(random.nextInt(products.length))
        val units   = randomInt(minUnits, maxUnits)

        try {
            val id:UUID = SQL(s"""
                |INSERT INTO "TransactionItem"
                |    ("cents", "units", "idTransaction", "idProduct")
                |VALUES
                |    ({cents}, {units}, {idTransaction}::uuid, {idProduct}::uuid)
            """.stripMargin.trim).
                asSimple().
                on(
                    "cents"         -> product._2 * units,
                    "units"         -> units,
                    "idTransaction" -> idTransaction,
                    "idProduct"     -> product._1
                ).
                executeInsert(SqlParser.scalar[UUID].single)
        } catch {
            case e : Exception => {
                Console.println(s"Failed to insert transaction #$i:  ${e.getClass.getSimpleName}:  ${e.getMessage}")
                sys.exit(exitFailure)
            }
        }
    }
}

Console.println()

Console.println("Done.")
sys.exit(exitSuccess)
