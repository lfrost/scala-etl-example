/*
 * Copyright (c) 2017-2021 Lyle Frost <lfrost@cnz.com>.
 */

package main

import java.sql.{Connection => DbConnection, DriverManager, PreparedStatement}
import java.time.{DayOfWeek, Month}

import scala.util.{Failure, Success, Try}

import anorm.{Column, MetaDataItem, ToStatement, TypeDoesNotMatch}
import org.postgresql.util.PGobject

/**
 *
 *  @param driver   JDBC driver name
 *  @param url      JDBC URL
 *  @param username database username
 *  @param password database password
 */
case class DbConnectionConfig(
    driver   : String = "",
    url      : String = "",
    username : String = "",
    password : String = ""
)

/** Execute SQL statements
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 */
object Sql {
    /** Custom conversion from JDBC citext column to String
     *
     *  @see [[https://playframework.github.io/anorm/#column-parsers]]
     */
    implicit val ColumnCitextToString : Column[String] = Column.nonNull { (value, meta) =>
        // Extract the column name and JDBC class name from the column meta data.
        val MetaDataItem(column, nullable@_, clazz) = meta

        value match {
            case s : String                                  => Right(s)
            case pgo : PGobject if pgo.getType() == "citext" => Try { pgo.getValue() } match {
                case Success(v) => Right(v)
                case Failure(e) => Left(TypeDoesNotMatch(s"Error $column::$clazz value $value to String: ${e.getMessage}"))
            }
            case _                                           => Left(TypeDoesNotMatch(s"Cannot convert $column::$clazz value $value to String."))
        }
    }

    /** Custom conversion from JDBC column to DayOfWeek
     *
     *  @see [[https://playframework.github.io/anorm/#column-parsers]]
     */
    implicit val ColumnIntToDayOfWeek : Column[DayOfWeek] = Column.nonNull { (value, meta) =>
        // Extract the column name and JDBC class name from the column meta data.
        val MetaDataItem(column, nullable@_, clazz) = meta

        value match {
            case i  : Int if (i >= 1 && i <= 7) => Right(DayOfWeek.of(i))
            case _                              => Left(TypeDoesNotMatch(s"Cannot convert $column::$clazz value $value to DayOfWeek."))
        }
    }

    /** Custom conversion to statement for type DayOfWeek
     *
     *  @see [[https://playframework.github.io/anorm/#custom-parameter-conversions]]
     */
    implicit val DayOfWeekToStatement : ToStatement[DayOfWeek] = new ToStatement[DayOfWeek] {
        override def set(statement:PreparedStatement, index:Int, value:DayOfWeek) : Unit = {
            if (value != null) {  // scalastyle:ignore null
                statement.setObject(index, value.getValue().toShort)
            } else {
                statement.setNull(index, java.sql.Types.SMALLINT)
            }
        }
    }

    /** Custom conversion from JDBC column to Month
     *
     *  @see [[https://playframework.github.io/anorm/#column-parsers]]
     */
    implicit val ColumnIntToMonth : Column[Month] = Column.nonNull { (value, meta) =>
        // Extract the column name and JDBC class name from the column meta data.
        val MetaDataItem(column, nullable@_, clazz) = meta

        value match {
            case i  : Int if (i >= 1 && i <= 12) => Right(Month.of(i))
            case _                               => Left(TypeDoesNotMatch(s"Cannot convert $column::$clazz value $value to Month."))
        }
    }

    /** Custom conversion to statement for type Month
     *
     *  @see [[https://playframework.github.io/anorm/#custom-parameter-conversions]]
     */
    implicit val MonthToStatement : ToStatement[Month] = new ToStatement[Month] {
        override def set(statement:PreparedStatement, index:Int, value:Month) : Unit = {
            if (value != null) {  // scalastyle:ignore null
                statement.setObject(index, value.getValue().toShort)
            } else {
                statement.setNull(index, java.sql.Types.SMALLINT)
            }
        }
    }

    /** Connect to a database
     *
     *  @param dcc database connection configuration
     *  @return    database connection or exception
     */
    def connect(dcc:DbConnectionConfig) : Try[DbConnection] = Try {
        Class.forName(dcc.driver)
        DriverManager.getConnection(dcc.url, dcc.username, dcc.password)
    }
}
