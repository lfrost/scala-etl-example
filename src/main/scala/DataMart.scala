/*
 * Copyright (c) 2017-2021 Lyle Frost <lfrost@cnz.com>.
 */

package main

import java.time.{DayOfWeek, LocalDate, Month}
import java.util.UUID

/** Daily sales fact
 *
 *  @param id                 ID,
 *  @param units              units sold
 *  @param cents              cents collected
 *  @param idDateDimension    date dimension ID
 *  @param idProductDimension product dimension ID
 *  @param idStoreDimension   store dimension ID
 */
case class DailySalesFact(
    id                 : Option[Long]  = None,
    cents              : Int,
    units              : Int,

    idDateDimension    : Option[Short] = None,
    idProductDimension : Option[UUID]  = None,
    idStoreDimension   : Option[Short] = None
)

/** Date dimension
 *
 *  @param id        ID
 *  @param date      date
 *  @param dayOfWeek day of week
 *  @param month     month
 *  @param year      year
 */
case class DateDimension(
    id        : Option[Short] = None,
    date      : LocalDate,
    dayOfWeek : DayOfWeek,
    month     : Month,
    year      : Int
)

/** Product dimension
 *
 *  @param id    ID
 *  @param gtin  GTIN
 *  @param name  name
 *  @param brand brand
 *  @param cents cents
 */
case class ProductDimension(
    id    : UUID,
    gtin  : String,
    name  : String,
    brand : Option[String] = None,
    cents : Int
)

/** Store dimension
 *
 *  @param id         ID
 *  @param code       unique code
 *  @param name       name
 *  @param city       city
 *  @param postalCode postal code
 *  @param region     region
 */
case class StoreDimension(
    id         : Option[Short] = None,
    code       : String,
    name       : String,
    city       : String,
    postalCode : String,
    region     : String
)
