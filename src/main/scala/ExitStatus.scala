/*
 * Copyright (c) 2017-2021 Lyle Frost <lfrost@cnz.com>.
 */

package main

/** Exit status values
 *
 *  This trait is for use with objects, so it follows the style
 *  guidelines for objects rather than classes.
 */
trait ExitStatus {
    // scalastyle:off field.name
    /** Success
     */
    val ExitSuccess = 0
    /** General error
     */
    val ExitFailure = 1
    /** Usage error
     */
    val ExitUsage   = 2
    // scalastyle:on field.name
}
