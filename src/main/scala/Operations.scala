/*
 * Copyright (c) 2017-2021 Lyle Frost <lfrost@cnz.com>.
 */

package main

import java.time.ZonedDateTime
import java.util.UUID

/** Store
 *
 *  @param id          ID
 *  @param whenCreated creation timestamp
 *  @param code        code
 *  @param name        name
 *  @param city        city
 *  @param postalCode  postal code
 *  @param region      region
 */
case class Store(
    id          : Option[UUID],
    whenCreated : Option[ZonedDateTime],
    code        : String,
    name        : String,
    city        : String,
    postalCode  : String,
    region      : String
)

/** Product
 *
 *  @param id          ID
 *  @param whenCreated creation timestamp
 *  @param upc         UPC
 *  @param name        name
 *  @param brand       brand
 */
case class Product(
    id          : Option[UUID],
    whenCreated : Option[ZonedDateTime],
    upc         : String,
    name        : String,
    brand       : Option[String]
)

/** Transaction
 *
 *  @param id          ID
 *  @param whenCreated creation timestamp
 *  @param idStore     store ID
 */
case class Transaction(
    id          : Option[UUID],
    whenCreated : Option[ZonedDateTime],
    idStore     : UUID
)

/** TransactionItem
 *
 *  @param id        ID
 *  @param cents     cents
 *  @param units     units
 *  @param idProduct product ID
 */
case class TransactionItem(
    id        : Option[UUID],
    cents     : Int,
    units     : Int,

    idProduct : UUID
)
