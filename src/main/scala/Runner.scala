/*
 * Copyright (c) 2017-2021 Lyle Frost <lfrost@cnz.com>.
 */

package main

import java.sql.{Connection => DbConnection}
import java.time.{DayOfWeek, LocalDate, Month}
import java.util.UUID

import scala.util.{Failure, Success, Try}

import Sql.{ColumnCitextToString, ColumnIntToDayOfWeek, ColumnIntToMonth, DayOfWeekToStatement, MonthToStatement}
import anorm.{~, ColumnAliaser, Row, RowParser, SQL}
import anorm.SqlParser.{get, scalar}
import anorm.postgresql.UUIDToSql

/** Runner state information
 *
 *  The runner maintains list of date IDs in a map of the form
 *  (DateDimension.date -> DateDimension.id) to eliminate unnecessary
 *  attempted inserts.  Similarly for stores, a map of the form
 *  (Store.code -> StoreDimension.id) is maintained.  A loop counter is
 *  also included.
 *
 *  For products, of which there are potentially a much larger number,
 *  no map is maintained.  An INSERT is attempted every time.
 *
 *  @param count      loop counter
 *  @param dates      date to operations database ID map
 *  @param storeCodes store code to operations database ID map
 */
case class RunnerState(
    count      : Long                  = 0,
    dates      : Map[LocalDate, Short] = Map.empty[LocalDate, Short],
    storeCodes : Map[String, Short]    = Map.empty[String, Short]
)

/** Singleton for processing a line of input.
 */
object Runner {
    private val ProgressInterval = 1000
    private val ProgressChar     = '.'

    /** The ETL query
     *
     *  The casts to int are included because EXTRACT returns a double.
     *
     *  Product.id is included because it will pass through and be used
     *  as the primary key in the data mart.
     */
    private val Query = """
        |SELECT
        |    sum("TransactionItem"."units")                              AS "units",
        |    sum("TransactionItem"."cents")                              AS "cents",
        |    "Transaction"."whenCreated"::date                           AS "date",
        |    EXTRACT(ISODOW FROM "Transaction"."whenCreated"::date)::int AS "dayOfWeek",
        |    EXTRACT(MONTH FROM "Transaction"."whenCreated"::date)::int  AS "month",
        |    EXTRACT(YEAR FROM "Transaction"."whenCreated"::date)::int   AS "year",
        |    "Product"."id", "Product"."gtin", "Product"."name", "Product"."brand", "Product"."cents",
        |    "Store"."code", "Store"."name", "Store"."city", "Store"."postalCode", "Store"."region"
        |FROM "Store"
        |INNER JOIN "Transaction"     ON "Transaction"."idStore" = "Store"."id"
        |INNER JOIN "TransactionItem" ON "TransactionItem"."idTransaction" = "Transaction"."id"
        |INNER JOIN "Product"         ON "Product"."id" = "TransactionItem"."idProduct"
        |GROUP BY "Store"."id", "Product"."id", "date"
    """.stripMargin.trim

    private val DailySalesFactRowParser : RowParser[DailySalesFact] = {
        get[Int]("cents") ~
        get[Int]("units") map {
            case cents ~ units =>
                DailySalesFact(cents = cents, units = units)
        }
    }

    private val DateDimensionRowParser : RowParser[DateDimension] = {
        get[LocalDate]("date")      ~
        get[DayOfWeek]("dayOfWeek") ~
        get[Month]("month")         ~
        get[Int]("year")            map {
            case date ~ dayOfWeek ~ month ~ year =>
                DateDimension(date = date, dayOfWeek = dayOfWeek, month = month, year = year)
        }
    }

    private val ProductDimensionRowParser : RowParser[ProductDimension] = {
        get[UUID]("Product.id")              ~
        get[String]("Product.gtin")          ~
        get[String]("Product.name")          ~
        get[Option[String]]("Product.brand") ~
        get[Int]("Product.cents")            map {
            case id ~ gtin ~ name ~ brand ~ cents =>
                ProductDimension(id = id, gtin = gtin, name = name, brand = brand, cents = cents)
        }
    }

    private val StoreDimensionRowParser : RowParser[StoreDimension] = {
        get[String]("Store.code")       ~
        get[String]("Store.name")       ~
        get[String]("Store.city")       ~
        get[String]("Store.postalCode") ~
        get[String]("Store.region")     map {
            case code ~ name ~ city ~ postalCode ~ region =>
                StoreDimension(code = code, name = name, city = city, postalCode = postalCode, region = region)
        }
    }

    private val CombinedRowParser : RowParser[(DailySalesFact, DateDimension, ProductDimension, StoreDimension)] = {
        DailySalesFactRowParser ~ DateDimensionRowParser ~ ProductDimensionRowParser ~ StoreDimensionRowParser map {
            case dsf ~ dd ~ pd ~ sd =>
                (dsf, dd, pd, sd)
        }
    }

    /** Retrieve complete list without streaming
     *
     *  @param dbConnection database connection
     *  @return             list of tuples containing daily sales fact, date dim, product dim, and store dim
     */
    def listNoStream()(implicit dbConnection:DbConnection) : List[(DailySalesFact, DateDimension, ProductDimension, StoreDimension)] = {
        SQL(Query).
            asSimple().
            as(CombinedRowParser.*)
    }

    // Process a single row from the query and return a new state.
    private def doRow(dailySalesFact:DailySalesFact, dateDimension:DateDimension, productDimension:ProductDimension, storeDimension:StoreDimension, runnerState:RunnerState)(implicit dbConnection:DbConnection) : Try[RunnerState] = Try {
        // Insert store dimension if new.
        val (storeId, storeCodes) = runnerState.storeCodes.get(storeDimension.code) match {
            case Some(id) => (id, runnerState.storeCodes)  // This store has already been saved.
            case None     => {
                // Add new store to the database and state storeCodes.
                val newId = SQL("""
                    |INSERT INTO "StoreDimension"
                    |    ("code", "name", "city", "postalCode", "region")
                    |VALUES
                    |    ({code}, {name}, {city}, {postalCode}, {region})
                """.stripMargin.trim).
                    asSimple().
                    on(
                        "code"       -> storeDimension.code,
                        "name"       -> storeDimension.name,
                        "city"       -> storeDimension.city,
                        "postalCode" -> storeDimension.postalCode,
                        "region"     -> storeDimension.region
                    ).
                    executeInsert(scalar[Int].single).
                    toShort

                (newId, runnerState.storeCodes + (storeDimension.code -> newId))
            }
        }

        /* Insert product dimension, let DBMS take care of duplicates.
         * The result of this will be 0 if dup, 1 if new record inserted.
         */
        SQL("""
            |INSERT INTO "ProductDimension"
            |    ("id", "gtin", "name", "brand", "cents")
            |VALUES
            |    ({id}, {gtin}, {name}, {brand}, {cents})
            |ON CONFLICT ON CONSTRAINT "pkProductDimension" DO NOTHING
        """.stripMargin.trim).
            asSimple().
            on(
                "id"    -> productDimension.id,
                "gtin"  -> productDimension.gtin,
                "name"  -> productDimension.name,
                "brand" -> productDimension.brand,
                "cents" -> productDimension.cents
            ).
            executeUpdate()

        // Insert date dimension if new.
        val (dateId, dates) = runnerState.dates.get(dateDimension.date) match {
            case Some(id) => (id, runnerState.dates)  // This date has alread been saved.
            case None     => {
                // Add new date to the database and state dates.
                val newId = SQL("""
                    |INSERT INTO "DateDimension"
                    |    ("date", "dayOfWeek", "month", "year")
                    |VALUES
                    |    ({date}, {dayOfWeek}, {month}, {year})
                """.stripMargin.trim).
                    asSimple().
                    on(
                        "date"      -> dateDimension.date,
                        "dayOfWeek" -> dateDimension.dayOfWeek,
                        "month"     -> dateDimension.month,
                        "year"      -> dateDimension.year
                    ).
                    executeInsert(scalar[Int].single).
                    toShort

                (newId, runnerState.dates + (dateDimension.date -> newId))
            }
        }

        // Insert daily sales fact.
        val dsfCount = SQL("""
            |INSERT INTO "DailySalesFact"
            |    ("cents", "units", "idDateDimension", "idProductDimension", "idStoreDimension")
            |VALUES
            |    ({cents}, {units}, {idDateDimension}, {idProductDimension}, {idStoreDimension})
        """.stripMargin.trim).
            asSimple().
            on(
                "cents"              -> dailySalesFact.cents,
                "units"              -> dailySalesFact.units,
                "idDateDimension"    -> dateId,
                "idProductDimension" -> productDimension.id,
                "idStoreDimension"   -> storeId
            ).
            executeUpdate()
        if (dsfCount != 1) {
            throw new Exception(s"DSF insert count $dsfCount != 1.")
        }

        runnerState.copy(count = runnerState.count + 1, dates = dates, storeCodes = storeCodes)
    }

    /** Process a line of input.
     *
     *  @param idcc input database connection configuration
     *  @param odcc output database connection configuration
     *  @return     text to display or an exception
     */
    def run(idcc:DbConnectionConfig, odcc:DbConnectionConfig) : Try[String] = {
        (Sql.connect(idcc), Sql.connect(odcc)) match {
            case (Success(iconn), Success(oconn)) =>
                val result = SQL(Query).
                    asSimple().
                    fold(RunnerState(), ColumnAliaser.empty) { (rs:RunnerState, row:Row) =>
                        // Echo a progress dot periodically.
                        if (rs.count % ProgressInterval == 0) Console.print(ProgressChar)

                        // Not using CombinedRowParser here in order to demonstrate combining row parsers on the fly.
                        row.as(DailySalesFactRowParser ~ DateDimensionRowParser ~ ProductDimensionRowParser ~ StoreDimensionRowParser) match {
                            case Success(dsf ~ dd ~ pd ~ sd) => {
                                doRow(dsf, dd, pd, sd, rs)(oconn) match {
                                    case Success(newRs) => newRs
                                    case Failure(e)     => throw e
                                }
                            }
                            case Failure(e)                  => throw e
                        }
                    }(iconn) match {
                        case Left(es)  => Failure(new Exception(es.toString))
                        case Right(rs) => Success(s"Processed ${rs.count} rows.")
                    }
                Console.println()
                iconn.close()
                oconn.close()
                result
            case (Failure(e), _)                  => {
                Console.println(s"Input database connection failed: ${e.getClass.getSimpleName} ${e.getMessage}")
                Failure(e)
            }
            case (_, Failure(e))                  => {
                Console.println(s"Input database connection failed: ${e.getClass.getSimpleName} ${e.getMessage}")
                Failure(e)
            }
        }
    }
}
