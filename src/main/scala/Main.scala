/*
 * Copyright (c) 2017-2021 Lyle Frost <lfrost@cnz.com>.
 */

package main

import scala.util.{Failure, Success}

/** CLI configuration
 *
 *  @param inDb    input database connection
 *  @param outDb   output database connection
 *  @param verbose Verbose flag
 */
case class CliConfig(
    inDb  : DbConnectionConfig = DbConnectionConfig(driver = "org.postgresql.Driver"),
    outDb : DbConnectionConfig = DbConnectionConfig(driver = "org.postgresql.Driver"),
    verbose : Boolean      = false
)

/** Starter template for Scala command-line applications.
 */
object Main extends App with ExitStatus {
    private val Pkg         = getClass.getPackage
    private val AppName     = Pkg.getImplementationTitle
    private val AppVersion  = Pkg.getImplementationVersion
    private val AppCommand  = AppName

    // CLI parser
    private val Parser = new scopt.OptionParser[CliConfig](AppCommand) {
        // Specify the application name and version for the help header.
        head(AppName, AppVersion)

        // Enable --help.
        help("help").
            text("prints this usage text")

        // Enable --version.
        version("version").
            text("display version then exit")

        opt[Unit]("verbose").
            text("verbose output").
            action((_, c) => c.copy(verbose = true))

        opt[String]('i', "inUrl").
            required().
            text("Input JDBC URL").
            action((x, c) => c.copy(inDb = c.inDb.copy(url = x)))

        opt[String]('u', "inUser").
            required().
            text("Input username").
            action((x, c) => c.copy(inDb = c.inDb.copy(username = x)))

        opt[String]('p', "inPassword").
            required().
            text("Input password").
            action((x, c) => c.copy(inDb = c.inDb.copy(password = x)))

        opt[String]('o', "outUrl").
            required().
            text("Output JDBC URL").
            action((x, c) => c.copy(outDb = c.outDb.copy(url = x)))

        opt[String]('v', "outUser").
            required().
            text("Output username").
            action((x, c) => c.copy(outDb = c.outDb.copy(username = x)))

        opt[String]('q', "outPassword").
            required().
            text("Output password").
            action((x, c) => c.copy(outDb = c.outDb.copy(password = x)))
    }

    // Parse the command line.
    Parser.parse(args, CliConfig()) match {
        case Some(cliConfig) => {
            Runner.run(cliConfig.inDb, cliConfig.outDb) match {
                case Success(s) => {
                    Console.println(s"ETL complete: $s")
                }
                case Failure(e) => {
                    Console.println(s"ETL failed: ${e.getMessage}")
                    sys.exit(ExitFailure)
                }
            }
        }
        case None            => {
            // Invalid command line.  Error message will have been displayed by scopt.
            sys.exit(ExitUsage)
        }
    }
}
