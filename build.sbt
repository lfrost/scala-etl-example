/*
 * Copyright (c) 2017-2021 Lyle Frost <lfrost@cnz.com>.
 */

import sbtassembly.AssemblyPlugin.defaultUniversalScript

ThisBuild / organization := "com.cnz"
ThisBuild / scalaVersion := "2.13.6"
ThisBuild / version      := "1.1.0"
ThisBuild / assemblyPrependShellScript := Some(defaultUniversalScript(shebang = false))

val gitBaseUrl = settingKey[String]("Git base URL")
gitBaseUrl := "https://gitlab.com/lfrost"

lazy val root = (project in file(".")).
    settings(
        name := "scala-etl-example",
        libraryDependencies ++= Seq(
            "org.postgresql"          %  "postgresql"     % "42.2.23",
            "com.github.scopt"        %% "scopt"          % "4.0.1",
            "org.playframework.anorm" %% "anorm"          % "2.6.10",
            "org.playframework.anorm" %% "anorm-postgres" % "2.6.10",
            "org.scalatest"           %% "scalatest"      % "3.2.9" % "test"
        ),
        // https://docs.scala-lang.org/overviews/compiler-options/
        scalacOptions ++= Seq(
            // Standard Settings
            "-deprecation",             // Emit warning and location for usages of deprecated APIs.
            "-encoding", "utf-8",       // Specify character encoding used by source files.
            "-explaintypes",            // Explain type errors in more detail.
            "-feature",                 // Emit warning and location for usages of features that should be imported explicitly.
            "-unchecked",               // Enable additional warnings where generated code depends on assumptions.

            // Advanced Settings

            // Warning Settings
            "-Ywarn-dead-code",         // Warn when dead code is identified.
            "-Ywarn-value-discard",     // Warn when non-Unit expression results are unused.
            "-Ywarn-numeric-widen",     // Warn when numerics are widened.
            "-Ywarn-unused:_",          // Enable or disable specific unused warnings: _ for all, -Ywarn-unused:help to list choices.
            "-Ywarn-extra-implicit",    // Warn when more than one implicit parameter section is defined.
            "-Xlint:_"                  // Enable or disable specific warnings: _ for all, -Xlint:help to list choices.
        ),
        Compile / doc / scalacOptions ++=
            Opts.doc.title(name.value)                                                                                             ++
            Opts.doc.sourceUrl(gitBaseUrl.value + "/" + name.value + "/tree/" + git.gitCurrentBranch.value + "€{FILE_PATH}.scala") ++
            Seq("-sourcepath", baseDirectory.value.getAbsolutePath),
        assembly / assemblyJarName := s"${name.value}-${version.value}",
        assembly / test := (Test / test).value,
    )
