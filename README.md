# scala-etl-example

This project is for the article [ETL With Scala and Native SQL](https://www.pinnsg.com/etl-scala-native-sql-part-1/).  It is an example of using Scala for an ETL (extract, transform, load) application.  It is forked from [scala-cli-starter](https://gitlab.com/lfrost/scala-cli-starter).


## System Setup for Scala Development

There are only two things needed for a local Scala development environment: a Java 8+ SDK (either OpenJDK or Oracle Java will work) and sbt.  sbt is a build tool like Gradle and Maven.  It is available at https://www.scala-sbt.org/download.html.

In order to run Scala scripts, also download and install a [Scala 2 binary](https://www.scala-lang.org/download/).


## Building

From the main directory of this application, run `sbt`.  The first time will take some time because it will download several things including a Scala compiler.  At the sbt prompt, run the following commands.

`assembly`
Builds an executable fat jar (which can be run directly, without `java -jar`) at target/scala-2.13/scala-etl-example-1.1.0.

`scalastyle`
Perform code style checks.

`exit`
Exits sbt.


## Database Setup

Save a password to be used for both databases in the file db/dbpass.

Run the following scripts to create the ETL example databases.  The createdb script requires Postgres 11, but should run on older versions if you remove  the \if statement for verifying that a password has been specified (lines 12-19).

```bash
psql -U postgres --set=password="$(< db/dbpass)" -f db/createdb.pg.sql
PGPASSWORD="$(< db/dbpass)" psql -U etl_operations -d etl_operations -f db/etl_operations.sql
PGPASSWORD="$(< db/dbpass)" psql -U etl_datamart -d etl_datamart -f db/etl_datamart.sql
```

The SQL script for each schema was generated with pgModeler.  The pgModeler files are also included in the db directory.


# Operations Data Generation

Some operations data will be needed for the application to extract.  A Scala script is included to do this.  There is also a short shell script that does the CLASSPATH setup for calling the Scala script.  The Scala script takes a product count and a transaction count as input, which can be changed in the shell script.

```bash
db/lib/download.sh
db/generate-operations.sh
```


## Running

The application can be executed the following command.  The JDBC URLs will have to change if the database is running on a different host or port.

```bash
target/scala-2.13/scala-etl-example-1.1.0                       \
    --inUrl 'jdbc:postgresql://localhost:5432/etl_operations'   \
    --inUser etl_operations                                     \
    --inPassword $(< db/dbpass)                                 \
    --outUrl 'jdbc:postgresql://localhost:5432/etl_datamart'    \
    --outUser etl_datamart                                      \
    --outPassword $(< db/dbpass)
```


## Relevant Documentation

* [sbt documentation](https://www.scala-sbt.org/learn.html)
* [Scala Standard Library API](https://www.scala-lang.org/api/2.13.6/)
* [Scala style guidelines](https://docs.scala-lang.org/style/)
* [Scalastyle rules](http://www.scalastyle.org/rules-1.0.0.html)
* [Anorm User Guide](https://playframework.github.io/anorm/)
* [Anorm API](https://playframework.github.io/anorm/unidoc/anorm/)
* [Postgres documentation](https://www.postgresql.org/docs/current/)
* [Postgres JDBC data type mapping](https://github.com/pgjdbc/pgjdbc/blob/master/pgjdbc/src/main/java/org/postgresql/jdbc/TypeInfoCache.java#L61)
* [pgModeler documentation](https://www.pgmodeler.io/support/docs)
