addSbtPlugin("com.eed3si9n"     % "sbt-assembly"          % "1.0.0")
addSbtPlugin("com.typesafe.sbt" % "sbt-git"               % "1.0.0")
addSbtPlugin("org.scalastyle"   % "scalastyle-sbt-plugin" % "1.0.0")
addSbtPlugin("org.scoverage"    % "sbt-scoverage"         % "1.8.2")
